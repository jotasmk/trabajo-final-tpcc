#include "filters.h"
#include <stdio.h>
#include <omp.h>

void gray_filter(float * gray, pixel_t * img, int ii, int jj)
{
	// Para acceder a cada elemento, tengo que usar que el archivo de la img viene como filas concatenadas
	#pragma omp parallel
	for (int i=0;i<ii;i++){
		for (int j=0;j<jj;j++){
			int offset = i*jj + j;											 // El elemento en la fila i y columna j tiene esta posicion en el array
			gray[offset]=(0.3*img[offset].r + 0.11*img[offset].b + 0.6*img[offset].g);
			}		
	}	
}
