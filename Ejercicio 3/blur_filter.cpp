#include "filters.h"
#include <stdio.h>
#include <omp.h>

void blur_filter(float * im_res, float * im, int ii, int jj)
{
	#pragma omp parallel
	// Para acceder a cada elemento, tengo que usar que el archivo de la img viene como filas concatenadas
	
	// Asigno valor 0 a los pixeles del borde de la imagen
	for (int i=0 ; i < ii ; i++){
		im_res[i*jj]=0;
	}
	for (int i=0 ; i < ii ; i++){
		im_res[i*jj+jj-1]=0;
	}
	for (int j=0 ; j < jj ; j++){
		im_res[j]=0;
	}
	for (int j=0 ; j < jj ; j++){
		im_res[((ii-1)*jj)+j]=0;
	}
	
	// Asigno el valor promedio al resto de los pixeles
	for (int i=1 ; i < ii-1 ; i++){
		for (int j=1 ; j < jj-1 ; j++){
			int offset = i*jj + j;			  // El elemento en la fila i y columna j tiene esta posicion en el array
			im_res[offset]=0.25*(im[offset-1] +im[offset+1] + im[offset+jj]+im[offset-jj]);
		}	
	
	}
}
