from scipy import misc
import numpy as np
import sys
import imfilters 
import time

funcion=sys.argv[1]
imagen=sys.argv[2]
imgsalida=sys.argv[3]
    
#gray_filter(imagen)
def promediado(img):
    dim=img.shape
    #Matriz corrida hacia abajo, con los elementos de su borde iguales a cero
    img_i1=img[:-1,:]
    tmp=np.zeros((1,dim[1]),dtype=int)
    img_i1=np.append(tmp,img_i1,axis=0)        #Agrego una primera fila de 0, y de esta forma "lo corri hacia abajo"
    img_i1[-1,:]=0
    img_i1[:,0]=0
    img_i1[:,-1]=0
    img_i1=img_i1*0.25                          #Ya hago el producto de la matriz por 0.25
    #Matriz corrida hacia arriba, con los elementos de su borde iguales a cero
    img_i2=img[1:,:]
    tmp=np.zeros((1,dim[1]),dtype=int)
    img_i2=np.append(img_i2,tmp, axis=0)        #Agrego una primera fila de 0, y de esta forma "lo corri hacia arriba"
    img_i2[0,:]=0
    img_i2[:,0]=0
    img_i2[:,-1]=0
    img_i2=img_i2*0.25
    #Matriz corrida hacia la derecha
    img_i3=img[:,:-1]
    tmp=np.zeros((dim[0],1),dtype=int)
    img_i3=np.append(tmp, img_i3,axis=1) 
    img_i3[0,:]=0
    img_i3[:,-1]=0
    img_i3[-1,:]=0
    img_i3=img_i3*0.25
    #Matriz corrida hacia la izquierda
    img_i4=img[:,1:]
    tmp=np.zeros((dim[0],1),dtype=int)
    img_i4=np.append( img_i4,tmp,axis=1) 
    img_i4[0,:]=0
    img_i4[:,0]=0
    img_i4[-1,:]=0
    img_i4=img_i4*0.25
    #Ahora, estas matrices se suman y dan como resultado la matriz deseada
    img_gris_filtrada=img_i1+img_i2+img_i3+img_i4
    return img_gris_filtrada

def gray_filter(a):
    #Abrir archivo
    imagen_array=np.array(misc.imread(a))
    #Hago un nuevo array en donde se sumen las intensidades segun la formula planteada
    a=imagen_array[:,:,0]*0.3+imagen_array[:,:,1]*0.6+imagen_array[:,:,2]*0.11
    #Devuelvo la imagen gris
    misc.imsave(imgsalida,a) 
    
def blur_filter(a):
    #Abrir archivo
    img=np.array((misc.imread(a)))
    img_gris_filtrada=promediado(img)
    misc.imsave(imgsalida,img_gris_filtrada) 

def gray_filter_con_blur_filter(a):
    t1=time.time()
    imagen_array=np.array(misc.imread(a))
    img=imagen_array[:,:,0]*0.3+imagen_array[:,:,1]*0.6+imagen_array[:,:,2]*0.11
    img_gris_filtrada=promediado(img)
    misc.imsave(imgsalida,img_gris_filtrada)
    t2=time.time()
    print("El programa implementado en Python demor�: " + str(t2-t1))
    
    
################### Usando implementacion en C++ #####################################



def gray_filter_cpp(a):
    #Abrir archivo
    imagen_array=np.array(misc.imread(a))
    #Llamo a la funcion hecha en C++
    imagen_gris=imfilters.gray_filter(imagen_array)
    #Devuelvo la imagen gris
    misc.imsave(imgsalida,imagen_gris) 

def blur_filter_cpp(a):
    img=np.array((misc.imread(a)))
    img_gris_filtrada=imfilters.blur_filter(img)
    misc.imsave(imgsalida,img_gris_filtrada) 
    
def gray_filter_cpp_con_blur_filter_cpp(a):
    t1=time.time()
    imagen_array=np.array(misc.imread(a))
    imagen_gris=imfilters.gray_filter(imagen_array)
    img_gris_filtrada=imfilters.blur_filter(imagen_gris)
    misc.imsave(imgsalida,img_gris_filtrada)
    t2=time.time()
    print("El programa implementado en C++ usado desde Python demor�: " + str(t2-t1))
    
################## La funcion a ejecutar se elije desde la terminal  ################

if funcion=="Python":
    gray_filter_con_blur_filter(imagen)

if funcion=="cpp":
    gray_filter_cpp_con_blur_filter_cpp(imagen)









