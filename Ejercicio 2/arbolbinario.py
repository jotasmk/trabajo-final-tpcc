
class ArbolBinario(object):
    
    def __init__(self):
        self.laraiz=None                              #Se puso "laraiz" en vez de "raiz" porque sino había un problema al definir la funcion raiz
        self.hijoizquierdo=None
        self.hijoderecho=None

    def vacio(self):
        if self.laraiz==None:
            return "El arbol no posee ningun elemento"
        else:
            return "El arbol posee elementos"

    def raiz(self):
        if self.laraiz==None:
            return None
        else:
            return self.laraiz
        

    def bin(self, a, izq, der):
        self.laraiz=a
        if type(izq)==type(self):
            self.hijoizquierdo=izq
        else:
            return "Izq debe ser un arbol binario"
        if type(der)==type(self):
            self.hijoderecho=der
        else:
            return "Der debe ser un arbol binario"
        
    def izquierda(self):
        return self.hijoizquierdo
    
    def derecha(self):
        return self.hijoderecho

    #La clave en este problema esta en usar recursion
    def find(self,a):
        #Caso base: mira la raiz
        if self.raiz()==None:
            return False
        if self.raiz()==a:
            return True
        #Si la raiz no coincide, mira (primero) que haya algun elemento a la izquierda, y luego pregunta si dicho objeto contiene al elemento "a"
        if self.izquierda()!=None and self.izquierda().find(a):
            return True
        #Si no hay elemento, sigue y hace lo mismo pero con el lado derecha
        if self.derecha()!=None and self.derecha().find(a):
            return True
        #Si ninguna de las ramas contiene al elemento "a", entonces no esta presente en el arbol
        return False

    def espejo(self):
        #Primero le pido que espeje su rama iquierda. Si no hay,procede a espejar la derecha
        if self.izquierda()!=None:
            self.izquierda().espejo()
        if self.derecha()!=None:
            self.derecha().espejo()   
        #Espejar significa intercambiar izq por derecha. 
        #En una hoja, devuelve la misma hoja, porque el argumento del if a continuacion es falso. 
        #Si no es una hoja, intercambia las posiciones de izquierda y derecha
        if self.izquierda()!=None or self.derecha()!=None:
            temp1=list([self.izquierda(),self.derecha()])
            self.hijoderecho=temp1[0]
            self.hijoizquierdo=temp1[1]
        return self

    def preorder(self):
        resultado=[]
        resultado.append(self.raiz())
        if self.izquierda()!=None and self.izquierda().raiz()!=None:
            resultado= resultado + self.izquierda().preorder()
        if self.derecha()!=None and self.derecha().raiz()!=None:
            resultado = resultado + self.derecha().preorder()
        return resultado

    def inorder(self):
        resultado=[]
        tmp=[]
        if self.izquierda()!=None and self.izquierda().raiz()!=None:
            resultado = resultado + self.izquierda().preorder()
        tmp.append(self.raiz())
        resultado = resultado + tmp
        if self.derecha()!=None and self.derecha().raiz()!=None:
            resultado = resultado + self.derecha().preorder()
        return resultado

    def posorder(self):
        resultado=[]
        tmp=[]
        if self.izquierda()!=None and self.izquierda().raiz()!=None:
            resultado = resultado + self.izquierda().preorder()
        if self.derecha()!=None and self.derecha().raiz()!=None:
            resultado = resultado + self.derecha().preorder()
        tmp.append(self.raiz())
        resultado = resultado + tmp
        return resultado
#----------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------
#Defino funciones que me permitan crear un arbol a partir de una lista que tenga un formato determinado

l=[["Raiz"],["Nodo 1-1","Nodo 1-2"],[None,"Nodo 2-2","Nodo 2-3","Nodo 2-4"],[None,None,"Hoja 3-3","Hoja 3-4","Hoja 3-5","Hoja 3-6","Hoja 3-7","Hoja 3-8"]]

def listaDeHojas(ls):
    #Armo una lista de "hojas", que en realidad van a ser arboles sin rama izquierda ni derecha
    hojas=[]
    niveles=len(ls)
    for i in range(0,2**(niveles-1)):
        arboli=ArbolBinario()
        if ls[niveles-1][i]!=None:
            arboli.bin(ls[niveles-1][i],None,None)
        else:
            arboli=ArbolBinario()
        hojas.append(arboli)
    return hojas

def primeraRama(ls,j,hojas):
    #Armo la lista de las ramas más externas, usando la lista de hojas
        # ls es la lista que se usa en la construccion del arbol
        # j es el nivel que le corresponde a la primera lista de ramas (= niveles-2)
    rama=[]
    subniveles=2**j
    for i in range(0,subniveles):
        if ls[j][i]==None:
            rama.append(ArbolBinario())
        else:
            a=ArbolBinario()
            a.bin(ls[j][i],hojas[2*i],hojas[2*i+1])
            rama.append(a)
    return rama

#A partir de una listas de listas voy a crear el arbol binario
def ConstructordelArbol(ls):
    niveles=len(ls)
    ramas=[]
    #Armo una lista de "hojas", que en realidad van a ser arboles sin rama izquierda ni derecha
    hojas=listaDeHojas(ls)
    #Creo la primera lista de ramas
    ramas.append(primeraRama(ls,niveles-2,hojas))
    #A partir de ahora,  vamos a ir obteniendo las ramas que contienen a las subramas
    #La idea es que el ultimo elemento de la lista ramas sea una lista con solo 2 elementos: la rama izquerda y la derecha que salen de la raiz del arbol
    for j in range(1,niveles-2):
        ramaj=[]
        for i in range(0,2**j):
            if ls[j][i]==None:
                ramaj.append(None)
            else:
                arbolinivelj=ArbolBinario()
                arbolinivelj.bin(ls[j][i],ramas[-1][2*i],ramas[-1][2*i+1])
                ramaj.append(arbolinivelj)
        ramas.append(ramaj)
    a=ArbolBinario()
    a.bin(ls[0][0],ramas[-1][0],ramas[-1][1])
    return a


print("Pruebas de la funcion ConstructordelArbol")
print("Luego de aplicar ConstructordelArbol(l).raiz() se obtiene: " + ConstructordelArbol(l).raiz())
print("Luego de aplicar ConstructordelArbol(l).izquierda().raiz() se obtiene: " + ConstructordelArbol(l).izquierda().raiz())
print("Luego de aplicar ConstructordelArbol(l).derecha().raiz() se obtiene: " + ConstructordelArbol(l).derecha().raiz())
print("Luego de aplicar ConstructordelArbol(l).izquierda().izquierda() se obtiene: " + str(ConstructordelArbol(l).izquierda().izquierda().raiz()))
print("Luego de aplicar ConstructordelArbol(l).izquierda().derecha().izquierda().raiz() se obtiene: " + ConstructordelArbol(l).izquierda().derecha().izquierda().raiz())

print("")
print("Pruebas de la funcion find(self)")
print("Luego de aplicar la funcion ConstructordelArbol(l).find(Hoja 3-6) se obtuvo: "+ str(ConstructordelArbol(l).find("Hoja 3-6")))
print("Luego de aplicar la funcion ConstructordelArbol(l).find(Nodo 2-1) se obtuvo: "+ str(ConstructordelArbol(l).find("Nodo 2-1")))
print("")

print("Pruebas de la funcion espejo")
print("Luego de aplicar ConstructordelArbol(l).espejo().raiz() se obtiene: " + ConstructordelArbol(l).espejo().raiz())
print("Luego de aplicar ConstructordelArbol(l).espejo().izquierda().raiz() se obtiene: " + ConstructordelArbol(l).espejo().izquierda().raiz())
print("Luego de aplicar ConstructordelArbol(l).espejo().derecha().raiz() se obtiene: " + ConstructordelArbol(l).espejo().derecha().raiz())
print("Luego de aplicar ConstructordelArbol(l).espejo().izquierda().izquierda() se obtiene: " + str(ConstructordelArbol(l).espejo().izquierda().izquierda().raiz()))
print("Luego de aplicar ConstructordelArbol(l).espejo().izquierda().derecha().izquierda().raiz() se obtiene: " + ConstructordelArbol(l).espejo().izquierda().derecha().izquierda().raiz())

print("")
  
print("Pruebas de las funciones preorder, posorder e inorder")  
a=ConstructordelArbol(l)
print("La lista preorder es:")
print(a.preorder())
print("")
print("La lista posorder es:")
print(a.posorder())
print("")
print("La lista inorder es:")
print(a.inorder())
