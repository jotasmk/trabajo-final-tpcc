import sys
import math
import time
import matplotlib.pyplot as plt
from operator import itemgetter

# Lee entrada
parametros=sys.argv
entradita=parametros[1] 

#Genera lista de listas de puntos (tuplas '(x, y)') a partir de archivo de entrada
def listaDePuntos(fn):
    entrada = open(fn)
    lista=[]
    for i in entrada:
        listatemporal=(i.strip('\n').split(' ')) #En cada linea los números vienen separados por un espacio
        punto=(float(listatemporal[0]), float(listatemporal[1])) #Para que los guarde como floats (vienen como strings originalmente)
        lista.append(punto)
    return lista



#-------------------------- Fuerza Bruta
#------------------------------------------------------


def distanciaMinima(a):
    distanciamin=math.sqrt(((a[0][0]-a[1][0])**2)+((a[0][1]-a[1][1])**2)) #Toma como minima la distancia entre los primeros dos puntos
    res=[a[0],a[1]] 
    for i in range(0,len(a)-1): #Barre del primero al anteúltimo elemento de a        
        for j in range(i+1,len(a)): #Calcula distancia del elemento iésimo con el todos los demás
            distanciaij=math.sqrt((a[i][0]-a[j][0])**2+(a[i][1]-a[j][1])**2)
            if distanciaij < distanciamin and distanciaij != 0.0: # Compara con el valor actual de distanciamin
                distanciamin = distanciaij  # Si la distancia ij es menor,la guarda como distanciamin
                res=[a[i],a[j]]
    return res

a=listaDePuntos(entradita)



#-------------------------- Divide & Conquer
#------------------------------------------------------

def mergex(a1, a2):
    i=0
    j=0 
    res=[] 
# Ordena inteligentemente ("intercala" elementos de secuencias ya ordenadas)
    while(i < len(a1) and j < len(a2)):
        if (a1[i][0] < a2[j][0]):
            res.append(a1[i])
            i += 1
        else:
            res.append(a2[j])
            j += 1
    res = res + a1[i:]
    res = res + a2[j:]
    return res

def mergey(a1, a2):
    i=0
    j=0
    res=[]
# Ordena inteligentemente ("intercala" elementos de secuencias ya ordenadas)
    while(i < len(a1) and j < len(a2)):
        if (a1[i][1] < a2[j][1]):
            res.append(a1[i])
            i += 1
        else:
            res.append(a2[j])
            j += 1
    res = res + a1[i:]
    res = res + a2[j:]
    return res

def mergeSortx(a):
    if len(a) <= 1:
        return a
    a1=mergeSortx(a[:(len(a)//2)+len(a)%2])
    a2=mergeSortx(a[(len(a)//2)+len(a)%2:])
    return mergex(a1, a2)

def mergeSorty(a):
    if len(a) <= 1:
        return a
    a1=mergeSorty(a[:(len(a)//2)+len(a)%2])
    a2=mergeSorty(a[(len(a)//2)+len(a)%2:])
    return mergey(a1, a2)

def upsortx(a):
    actual = len(a)-1
    m = 0
    while (actual > 0):
        m = maxPosx(a,0,actual)
        a[m], a[actual] = a[actual] , a[m]
        actual = actual - 1

def upsorty(a):
    actual = len(a)-1
    m = 0
    while (actual > 0):
        m = maxPosy(a,0,actual)
        a[m], a[actual] = a[actual] , a[m]
        actual = actual - 1


# Devuelve posición del máximo (en x) entre los elementos ai y af de la lista a
def maxPosx(a,ai,af):
    res = af  #asume que el elemento af es el máximo
    for i in range(ai,af): #barre desde ai hasta uno antes que af
        if a[i][0] > a[res][0]:
            res = i
    return res

def maxPosy(a,ai,af):
    res = af  #asume que el elemento af es el máximo
    for i in range(ai,af): #barre desde ai hasta uno antes que af
        if a[i][1] > a[res][1]:
            res = i
    return res


#-------------------------------------------------------------------


def distDosPuntos(a): #Recibe lista de dos puntos
    return math.sqrt((a[0][0]-a[1][0])**2+(a[0][1]-a[1][1])**2)

def sortordenadoseny(a,b,algoritmo):
    if algoritmo == "merge": #Ordena según el valor de "algoritmo"
        a=mergeSorty(a)
    if algoritmo == "up":
        upsorty(a)
    if algoritmo == "python":
        a.sort(key=itemgetter(1))
    if len(a) <= 3:
        return b
    else:
#        a=mergeSorty(a)
        d=distDosPuntos(b)
        for i in range(0,len(a)):
            j=1
            while (a[i][1]-a[j][1]) < d:
                distanciaij=math.sqrt((a[i][0]-a[j][0])**2+(a[i][1]-a[j][1])**2)
                if  distanciaij < d:
                    d = distanciaij
                    b = [a[i],a[j]]
                j=j-1
    return b

def distanciaMinimaValue(a):
    distanciamin=math.sqrt(((a[0][0]-a[1][0])**2)+((a[0][1]-a[1][1])**2)) #Toma como minima la distancia entre los primeros dos puntos
    res=[a[0],a[1]]
    for i in range(0,len(a)-1): #Barre del primero al anteúltimo elemento de a        
        for j in range(i+1,len(a)): #Calcula distancia del elemento iésimo con el todos los demás
            distanciaij=math.sqrt((a[i][0]-a[j][0])**2+(a[i][1]-a[j][1])**2)
            if distanciaij < distanciamin and distanciaij != 0.0: # Compara con el valor actual de distanciamin
                distanciamin = distanciaij  # Si la distancia ij es menor,la guarda como distanciamin
                res=[a[i],a[j]]
    return distanciamin

def distanciaMinimaDyC(a, algoritmo):
    if algoritmo == "merge": #Ordena según el valor de "algoritmo"
        a=mergeSortx(a)
    if algoritmo == "up":
        upsortx(a)
    if algoritmo == "python":
        a.sort(key=itemgetter(0))

    if len(a) <= 4: #Caso base lo resuelve por fuerza bruta
        return distanciaMinima(a)
    else: #Divide 
        a1=a[:len(a)//2] 
        a2=a[len(a)//2:]
        mina1=distanciaMinimaDyC(a1, algoritmo)
        mina2=distanciaMinimaDyC(a2, algoritmo)
        if distDosPuntos(mina1) < distDosPuntos(mina2):
            mina12=mina1
        else:
            mina12=mina2
        mina12Value=distDosPuntos(mina12)
        i=len(a)//2
        j=len(a)//2
        while ((i < len(a)) and ((a[i][0]-a[len(a)//2][0]) < mina12Value)):
            i = i + 1 
        while ((j >= 0) and ((a[len(a)//2][0]-a[j][0]) < mina12Value)):
            j = j - 1
        a3=a[j+1:i]
        return sortordenadoseny(a3,mina12,algoritmo)  

#----------------------------------------- Graficos pedidos
#----------------------------------------------------------

#---- Fuerza bruta

z = listaDePuntos(entradita)
N = len(z)
i = 100
EvolucionTemporal_FuerzaBruta = []
while i <= N:
    z_cut=z[:i]
    t1=time.time()
    b=distanciaMinima(z_cut)
    t2=time.time()
    punto = (i,t2-t1)
    EvolucionTemporal_FuerzaBruta.append(punto)
    i = i +100

x_val = [x[0] for x in EvolucionTemporal_FuerzaBruta]
y_val = [x[1] for x in EvolucionTemporal_FuerzaBruta]

plt.plot(x_val,y_val,linewidth=1.0, label= 'Fuerza Bruta')
plt.ylabel('Tiempo (s)')
plt.xlabel('N')
plt.legend()
plt.savefig('Figura1.png')

#---- Divide & Conquer - MergeSort

z = listaDePuntos(entradita)
N = len(z)
i = 100
EvolucionTemporal_DyC_MergeSort = []
while i <= N:
    z_cut=z[:i]
    t1=time.time()
    b=distanciaMinimaDyC(z_cut,"merge")
    t2=time.time()
    punto = (i,t2-t1)
    EvolucionTemporal_DyC_MergeSort.append(punto)
    i = i +100

x_val = [x[0] for x in EvolucionTemporal_DyC_MergeSort]
y_val = [x[1] for x in EvolucionTemporal_DyC_MergeSort]

plt.plot(x_val,y_val,linewidth=1.0, label= 'DyC - MergeSort')
plt.legend()

#---- Divide & Conquer - UpSort

z = listaDePuntos(entradita)
N = len(z)
i = 100
EvolucionTemporal_DyC_UpSort = []
while i <= N:
    z_cut=z[:i]
    t1=time.time()
    b=distanciaMinimaDyC(z_cut,"up")
    t2=time.time()
    punto = (i,t2-t1)
    EvolucionTemporal_DyC_UpSort.append(punto)
    i = i +100

x_val = [x[0] for x in EvolucionTemporal_DyC_UpSort]
y_val = [x[1] for x in EvolucionTemporal_DyC_UpSort]

plt.plot(x_val,y_val,linewidth=1.0, label= 'DyC - UpSort')
plt.legend()

#---- Divide & Conquer - Python

z = listaDePuntos(entradita)
N = len(z)
i = 100
EvolucionTemporal_DyC_Python = []
while i <= N:
    z_cut=z[:i]
    t1=time.time()
    b=distanciaMinimaDyC(z_cut,"python")
    t2=time.time()
    punto = (i,t2-t1)
    EvolucionTemporal_DyC_Python.append(punto)
    i = i +100

x_val = [x[0] for x in EvolucionTemporal_DyC_Python]
y_val = [x[1] for x in EvolucionTemporal_DyC_Python]

plt.plot(x_val,y_val,linewidth=1.0, label= 'DyC - Python')
plt.ylabel('Tiempo (s)')
plt.xlabel('N')
plt.legend()
plt.savefig('Figura-Ej1.png')
plt.show()


