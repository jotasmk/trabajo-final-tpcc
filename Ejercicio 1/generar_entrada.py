import random
import sys

# Lee entrada
parametros=sys.argv
numerodepuntos=int(parametros[1])


def generarlistadepuntos(numerodepuntos):
    listadepuntos=[]
    for i in range(0,numerodepuntos):
        punto=(round(((-1)**int(10*random.random()))*10.0*random.random(), 3),round(((-1)**int(10*random.random()))*10.0*random.random(), 3))
        listadepuntos.append(punto)
    return listadepuntos


def escribirsalida(listadepuntos):
    salida=open('entrada_generada','w')
    for i in range(0,len(listadepuntos)):
        print(listadepuntos[i][0], listadepuntos[i][1], file=salida)
    salida.close
    return

listadepuntos=generarlistadepuntos(numerodepuntos)
escribirsalida(listadepuntos)
