# -*- coding: utf-8 -*-

class Laberinto(object):
    def __init__(self, parent=None):
        self._dimension = (0,0)  #Inicializo dimension para evitar errores al abrir tplaberinto.py (que de todas formas no afectan el funcionamiento del programa)
        self.parent = parent

#-------------------------- Interfaz (métodos publicos)
#------------------------------------------------------

    def cargar(self, fn):
        self._entrada = open(fn)
        self._dimension=next(self._entrada)
        self._dimension=self._dimension.strip('Dim').strip('\n').strip('(').strip(')').split(',')
        self._dimension=(int(self._dimension[0]) , int(self._dimension[1]))
        self._filas=self._dimension[0]
        self._columnas=self._dimension[1]
        self._laberinto=[]
        for i in self._entrada: #Barro filas
            filai = []
            temp = i.replace('][', '] [') #Separo celdas con espacios
            temp = (temp.strip('\n').split(' ')) #Armo lista donde cada elemento es una celda (aún en formato de string)
            for j in range(0,self._columnas): #Para la fila i, barro cada columna j
                celdaij = []
                paredesij=temp[j].strip('[').strip(']').split(',') #Es una lista de 4 elementos, cada uno una pared (aún en formato de string)
                for k in range(0,4):
                    celdaij.append(int(paredesij[k])) #Celdaij tiene 4 elementos, cada uno una pared (enteros)
                celdaij = celdaij + [0,0] #Celdaij tiene ahora 6 elementos: 4 paredes + "Visitada" + "Camino actual"
                filai.append(celdaij)
            self._laberinto.append(filai)
        self._PosicionRata=(0,0)
        self._PosicionQueso=(self._filas-1,self._columnas-1)
    
    def resetear(self):
        for i in range(0,self._filas):
            for j in range(0,self._columnas):
                self._laberinto[i][j][4] = 0
                self._laberinto[i][j][5] = 0
        return "El laberinto fue resteado"

    def tamano(self): 
        return self._dimension

    def getPosicionRata(self):
        return self._PosicionRata

    def getPosicionQueso(self):
        return self._PosicionQueso

    def setPosicionRata(self,i,j):
        if (i >= 0) and (i < self._filas) and (j >= 0) and (j < self._columnas):
            self._PosicionRata=(i,j)
            return True
        return False
        
    def setPosicionQueso(self,i,j):
        if (i >= 0) and (i < self._filas) and (j >= 0) and (j < self._columnas):
            self._PosicionQueso=(i,j)
            return True
        return False

    def esPosicionRata(self,i,j):
        if self._PosicionRata == (i,j):
            return True
        return False


    def esPosicionQueso(self,i,j):
        if self._PosicionQueso == (i,j):
            return True
        return False
            
    def get(self,i, j):
        res=[]
        for k in range(0,4):
            res.append(self._laberinto[i][j][k] == 1)
        return res

    def getInfoCelda(self,i,j):
        visitada = (self._laberinto[i][j][4] == 1) 
        caminoActual = (self._laberinto[i][j][5] == 1)
        res = {'visitada': visitada, 'caminoActual': caminoActual}
        return res

    def resuelto(self):
        return (self._PosicionRata == self._PosicionQueso)


#-------------------------- Resuelvo por backtracking
#------------------------------------------------------


    def resolver(self):
        self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][5] = 1 #Marca como "actual" la celda donde está la rata
#        self._redibujar()
        self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][4] = 1 #Marca como "visitada" la celda donde está la rata
        self._redibujar()
        save = (self._PosicionRata[0], self._PosicionRata[1])
        if self.resuelto():  #Caso base: Está resuelto
            return True

        if self._PosicionRata[1] > 0:
            if self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][0] == 0 and self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]-1][4] == 0: #Si no hay pared izq 
                self.setPosicionRata(self._PosicionRata[0],self._PosicionRata[1]-1) #Movete a la izq
                self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][4] = 1 #Marca como "visitada" la celda donde está la rata
                self._redibujar()
                if self.resolver() == True:
                    return True
                else:
                    self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][5] = 0 #Desmarca como "actual" la celda donde está la rata
                    self.setPosicionRata(save[0],save[1]) #Vuelve atrás
                    self._redibujar()


        if self._PosicionRata[0] > 0:
            if self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][1] == 0 and self._laberinto[self._PosicionRata[0]-1][self._PosicionRata[1]][4] == 0: #Si no hay pared arriba
                self.setPosicionRata(self._PosicionRata[0]-1,self._PosicionRata[1]) #Movete arriba
                self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][4] = 1 #Marca como "visitada" la celda donde está la rata
                self._redibujar()
                if self.resolver() == True:
                    return True
                else:
                    self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][5] = 0 #Desmarca como "actual" la celda donde está la rata
                    self.setPosicionRata(save[0],save[1]) #Vuelve atrás
                    self._redibujar()


        if self._PosicionRata[1] < (self._columnas-1):
            if self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][2] == 0 and self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]+1][4] == 0: #Si no hay pared derecha
                self.setPosicionRata(self._PosicionRata[0],self._PosicionRata[1]+1) #Movete a la derecha
                self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][4] = 1 #Marca como "visitada" la celda donde está la rata
                self._redibujar()
                if self.resolver() == True:
                    return True
                else:
                    self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][5] = 0 #Desmarca como "actual" la celda donde está la rata
                    self.setPosicionRata(save[0],save[1]) #Vuelve atrás
                    self._redibujar()


        if self._PosicionRata[0] < (self._filas-1):
            if self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][3] == 0 and self._laberinto[self._PosicionRata[0]+1][self._PosicionRata[1]][4] == 0: # Si no hay pared abajo
                self.setPosicionRata(self._PosicionRata[0]+1,self._PosicionRata[1]) #Movete abajo
                self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][4] = 1 #Marca como "visitada" la celda donde está la rata
                self._redibujar()
                if self.resolver() == True:
                    return True
                else:
                    self._laberinto[self._PosicionRata[0]][self._PosicionRata[1]][5] = 0 #Desmarca como "actual" la celda donde está la rata
                    self.setPosicionRata(save[0],save[1]) #Vuelve atrás
                    self._redibujar()

        return False


#-------------------------- Interfaz (métodos privados)
#------------------------------------------------------

    def _redibujar(self):
        if self.parent is not None:
            self.parent.update()

